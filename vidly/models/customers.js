const mongoose = require('mongoose');
const Joi = require('joi');

const Customer = new mongoose.model('Customer',new mongoose.Schema(
    {
        name:{
            type:String,
            required:true,
            minlength:5,
            maxlength:15
        },
        phone:String,
        isGold:Boolean
    }
));

function validateCustomer(customer){
    const schema = {
        name: Joi.string().min(3).required(),
        phone:Joi.string().required(),
        isGold:Joi.boolean()
    };
    
    return(Joi.validate(customer,schema));
}

exports.Customer=Customer;
exports.validate= validateCustomer;