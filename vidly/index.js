const mongoose = require('mongoose');
const genres = require('./routes/genres');
const customers = require('./routes/customers');
const express = require('express');
const app = express();

mongoose.connect('mongodb://localhost/vidly')
.then(()=> console.log('connected to bd'))
.catch(err=>console.log('could not connects to bd'));
app.use(express.json());
app.use('/api/vidly/genres',genres);
app.use('/api/vidly/customers',customers);
const port = process.env.PORT || 3000;
app.listen(port,()=> console.log('listening on port '+port+' ...'))