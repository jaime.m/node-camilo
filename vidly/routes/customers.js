const {Customer,validate} = require('../models/customers')
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();


router.get('/',async(req,res)=>{
    try{
         const customers = await Customer.find().sort('name');
    res.send(customers); 
    }catch(err){
        console.log('exception'+ err.message);
    }
  
});


router.get('/:id',async(req,res)=>{
    try{
           const customer = await Customer.findById(req.params.id);
    res.send(customer);
    }catch(err){
console.log(err.message);
    }

});

router.post('/',async(req,res)=>{

    const {error} =validate(req.body);
    if(error){
      return res.status(400).send(error.details[0].message);
    }
    
    let customer = new Customer({
        name:req.body.name,
        phone:req.body.phone,
        isGold:req.body.isGold
    });
    customer = await customer.save();
    res.send(customer);
});

router.put('/:id',async(req,res)=>{
    const {error} = validate(req.body);
    if(error){
        return res.status(400).send(error.details[0].message);
    }

const customer = await Customer.findByIdAndUpdate(req.params.id,
    {
        name:req.body.name,
        phone:req.body.phone,
        isGold:req.body.isGold
    }, {new:true});
    if(!customer){
        return res.status(404).send('no se encontro el cliente para editar');        
    }

    res.send(customer);
});


router.delete('/:id',async(req,res)=>
{

    const customer = await Customer.findByIdAndDelete(req.params.id);
    const {error} =validate(req.body);
  if(!customer){
        return res.status(404).send('no se encontro el genero papa');

    }
    res.send(customer);

});



module.exports = router;