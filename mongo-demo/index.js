const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
.then(() => console.log('connected to MongoDB...'))
.catch(err => console.error('could no connect '+err.message));

const courseSchema = new mongoose.Schema(
    {
        name:{
            type:String,required:true,minlength:5,maxlength:255
        },
        category:{
            type:String,
            enum:['web','mobile','network']
        },
        author:String,
        tags:{
            type:Array,
            validate: {
                isAsync:true,
                validator:function(v,callback){
                   //Do some async work

                   setTimeout(() => {
                       const result =  v && v.length>0;
                       callback (result);
                   }, 200);
                
                }, message:'A course should have at least 1 tag'
            }
        },
        date:{type:Date,default:Date.now},
        isPublished:Boolean,
        price:{
            type:Number,
            required:function(){
                return this.isPublished;
            }
        }
    }
);

//Classes, objects
//Human, john
//this is a class pascal notation
const Course = mongoose.model('Course',courseSchema);


async function createCourse(){
    //this is a object
const course = new Course(
    {
      name:'Angular Course',
        author:'Mosh',
        tags:null,
        isPublished:false,
        price:15,
        category:'-'

    }
);
console.log('buenas tardes');
try{

  //await course.validate();
    
    const result = await course.save();
    console.log(result);

}catch(err){
    for(field in err.errors){
    console.log(err.errors[field].message);
}
}

}
async function getcourses(){
//eq(equal)
// ne (not equal)
//gt (greater than)
// gte (greater or equal than)
//lt (less than)
//lte (less or equal than)
//in 
//nin (not in)

    //1 ascending -1 descending
    const pageNumber =2;
    const pageSize=10;
    const courses = await Course
    //.find({price:{$gte:10,$lte:20}})
    //.or([{author:'Mosh'},{isPublished:true}])
    //.find({author:'Mosh', isPublished:true})
    //starts with mosh
    //.find({author:/^Mosh/})
    //ends with hamedani and not key sensitive
    //.find({author:/Hamedani$/i})
    //contains mosh
    .find({author:/.*Mosh.*/i})
    .limit(10)
    .sort({name:1})
   // .select({name:1, tags:1});
   .count();
    console.log(courses);
}

async function updateCourses(id)
{
//queryfirst
//find by id
//modify properties
//save()
const course = await Course.findById(id);
/*
if(!course)return;
course.isPublished=true;
course.author='another author';
const result = await course.save();
console.log(result);*/
//update first
//update directly
//get the document

const result = await Course.findByIdAndUpdate({_id:id},
    {$set:{
        author:'copyta',
        isPublished:false
    }
    },{new:true})
}

//updateCourses('5d1a6fd00e5c1530f80f3b56');
createCourse();



async function removeCourses(id)
{

const course = await Course.findById(id);

const result = await Course.findByIdAndDelete({_id:id},
    {$set:{
        author:'copyta',
        isPublished:false
    }
    },{new:true})
}