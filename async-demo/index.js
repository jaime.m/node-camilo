/*console.log('Before');
getUser(1, (user)=>getusers);

function getusers(user){
    getRepositories(user.gitHubUserName,getRepo);
}
function getRepo(repos){console.log('repos'+ repos);}
console.log('after');
*/
/*

 getUser(1)
 .then(user => getRepositories(user.gitHubUserName))
 .then(repo=> getCommits(repo[0]))
 .then(commits=> console.log('commits',commits))
 .catch(err=>console.log('Error',err.message));
 */
 //callbacks
//promises
//async/await

//Async and await

async function displayCommits(){
    try{
         const user = await getUser(1);
        const repos = await getRepositories(user.gitHubUserName);
        const commits = await getCommits(repos[0]);
        console.log(commits);

    }catch (err){
        console.log('error '+err.message);
    }
   
}

displayCommits();

function getUser(id){
return new Promise((resolve,reject)=>{
   setTimeout(()=>{
        console.log('reading from a database getting '+ id);
        resolve({id:id, gitHubUserName:'copyta'})
    },2000);
})
 
}

function getRepositories(username){
   return new Promise((resolve,reject)=>{
    setTimeout(()=>{
            console.log('reading from a github repository from '+ username);
            resolve(['repo','pero1','repo2'])
        },2000);
   });
   
}

function getCommits(repo){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
                console.log('reading commits of '+ repo);
                resolve(['commit'])
            },2000);
       });
}
