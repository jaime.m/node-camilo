const statupDebugger = require('debug')('app:startup');
const dbDebugger = require('debug')('app:db');
const config= require('config');
const morgan =require('morgan');
const helmet = require('helmet');

const logger = require('./middleware/logger');
const courses = require('./routes/courses')
const home = require('./routes/home')
const express = require('express');
const app = express();

app.set('view engine', 'pug');
app.set('views','./views');//default
app.use('/api/courses',courses);
app.use('/',home);
app.use(express.json()); 
app.use(express.urlencoded({extended: true})); 
app.use(express.static('public'));
app.use(helmet());

console.log('application name' + config.get('name'));
console.log('mail server' + config.get('mail.host'));
console.log('mail server' + config.get('mail.password'));
//log off the services
if(app.get('env')==='development'){
    app.use(morgan('tiny'));
    statupDebugger('morgan enabled....');
}
/**
 * Se puede ver el debug de las aplicaciones con los vbles de entorno, DEBUG en la consola
 * y se puede llamar así DEBUG = app:db nodemon index.js
 */
//db work
dbDebugger('connected to the DB...');

app.use(logger)





//PORT
const port = process.env.PORT || 3000;
app.listen(port, ()=> console.log('listening on port '+port+' papa'));