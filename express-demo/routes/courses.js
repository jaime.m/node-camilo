
const express = require('express');
const router= express.Router();
const Joi = require('joi');

router.use(express.json()); 

const courses = [
    {  id:1,name:'c1'  },
    
    {  id:2,name:'c2'  },
    
    {  id:3,name:'c3'  },
]

router.get('/',(req,res)=>{
    res.send(courses);
}
);
router.get('/:id',(req,res)=>{
  let course= courses.find(c=>c.id===parseInt(req.params.id));
  if(!course){
      res.status(404).send('no se encontro el curso papa')
  } 
  res.send(course);
}
);

router.get('/api/post/:year/:month',(req,res)=>{
    res.send(req.query);
}
);

router.post('/',(req,res)=>
{   
    console.log(req.body);
    const { error } = validateCourse(req.body);
    if (error){
    //400 bad request
        res.status(400).send(error.details[0].message);
        return;
    }
    const course={
    id:courses.length+1,
    name:req.body.name
    };
    courses.push(course);
    res.send(course);
});

router.put('/:id',(req,res)=>
{

//look up the course
//if not existing, 404

const { error } =validateCourse(req.body);
let course= courses.find(c=>c.id===parseInt(req.params.id));

console.log(courses);
if(!course){
  return  res.status(404).send('no se encontro el curso papa')

} 

//if invalid 400 bad request


if (error){
    //400 bad request
       return res.status(400).send(error.details[0].message);
        
    }

//update course

course.name = req.body.name;
res.send(course);

});

router.delete('/:id',(req,res)=>
{
//look up the course
//if not existing, 404

const { error } =validateCourse(req.body);
let course= courses.find(c=>c.id===parseInt(req.params.id));
if(!course){
   return res.status(404).send('no se encontro el curso papa')
} 

//delete course

const index = courses.indexOf(course);
courses.splice(index,1);
res.send(course);

});


function validateCourse(course){
    const schema = {
        name: Joi.string().min(3).required()
    };
    
    return(Joi.validate(course,schema));
}

module.exports = router;